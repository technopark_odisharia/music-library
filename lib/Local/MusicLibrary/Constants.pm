package  Local::MusicLibrary::Constants;

use strict;
use warnings;

our %FIELDS_TYPES = (
    'band'      =>  'string',
    'year'      =>  'numeric',
    'album'     =>  'string',
    'track'     =>  'string',
    'format'    =>  'string'
);

our @FIELDS = (
    'band',
    'year',
    'album',
    'track',
    'format'
);

our @OPTFIELDS = (
    'sort',
    'columns'
);

our %CALLBACKS = (
    'filter' => {
        'numeric' => sub {
            return $_[0] == $_[1]
        },
        'string' => sub {
            return $_[0] eq $_[1]
        },
    },

    'sort' => {
        'numeric' => sub {
            my ($library, $options) = @_;
            @{$library} = sort {
                ${$a}{${$options}{'sort'}} <=> ${$b}{${$options}{'sort'}}
            } @{$library}
        },
        'string' => sub {
            my ($library, $options) = @_;
            @{$library} = sort {
                ${$a}{${$options}{'sort'}} cmp ${$b}{${$options}{'sort'}}
            } @{$library}
        },
    },
);

1;
