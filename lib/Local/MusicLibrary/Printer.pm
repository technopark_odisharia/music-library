package Local::MusicLibrary::Printer;

use strict;
use warnings;
use List::Util qw(sum);
use Local::MusicLibrary::Constants;
use Exporter 'import';
our @EXPORT_OK = qw(print_table);

# DEBUG
use DDP;

our @OPTFIELDS      = @Local::MusicLibrary::Constants::OPTFIELDS;
our %FIELDS_TYPES   = %Local::MusicLibrary::Constants::FIELDS_TYPES;
our %CALLBACKS      = %Local::MusicLibrary::Constants::CALLBACKS;

sub filter_library {
    my ($library, $filters) = @_;

    my $temp = [ grep {
            my $temp = $_;
            grep {
                ${$CALLBACKS{'filter'}}{$FIELDS_TYPES{$_}}->(${$temp}{$_}, ${$filters}{$_});
            } keys %{$filters};
        } @{$library} ];

    ( @{$temp} || %{$filters} ) ? ( return $temp ) : ( return $library )
}

sub sort_library {
    my ($library, $options) = @_;

    ${$CALLBACKS{'sort'}}{$FIELDS_TYPES{${$options}{'sort'}}}->(@_) if ${$options}{'sort'};

    return $library;
}

sub make_columns {
    my ($library, $columns) = @_;

    foreach (@{$library}) {
        my $entry = $_;
        foreach (keys %{$_}) {
            ${$columns}{$_} = length(${$entry}{$_}) if ( length(${$entry}{$_}) > ${$columns}{$_});
        }
    }

    return $library
}

sub printer {
    my ($table, $colwidth, $fields) = (@_);

    return unless @{$table};

    my @defises = map { "-" x (${$colwidth}{$_} + 2 ) } @{$fields};
    my $separator = "|".join("+", @defises)."|\n";
    my $pattern = join("", map { sprintf("| %%%1ss ", ${$colwidth}{$_}) } @{$fields})."|\n";

    my $out = "/".join("-", @defises)."\\\n";
    $out .= join($separator, map {
        my $line = $_;
        sprintf($pattern, map({ ${$line}{$_} } @{$fields}));
    } @{$table});
    print($out .= ( "\\".join("-", @defises)."/\n" ));
}

sub print_table {
    my ($library, $filters, $options) = @_;

    return unless @{${$options}{'columns'}};

    my %colwidth;
    $colwidth{$_} = 0 for keys %{${$library}[0]};

    printer( make_columns( sort_library( filter_library(
                        $library, $filters),
                    $options),
                \%colwidth),
            \%colwidth,
            ${$options}{'columns'});
}

1;
