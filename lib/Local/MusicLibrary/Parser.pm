package Local::MusicLibrary::Parser;

use strict;
use warnings;

use Getopt::Long qw(GetOptions);
use Local::MusicLibrary::Constants;
use Exporter 'import';
our @EXPORT_OK = qw(get_options get_library);

# DEBUG
use DDP;

our @FIELDS = @Local::MusicLibrary::Constants::FIELDS;
our @OPTFIELDS = @Local::MusicLibrary::Constants::OPTFIELDS;

my %options = (
    'columns' => [
        @FIELDS
    ]
);
my %filters = ();
my %ACCEPTABLE_COLUMNS;
@ACCEPTABLE_COLUMNS{$_} = 'exists' for @FIELDS;

sub get_options {
    GetOptions(
        \%filters,
        "band=s", "year=i", "album=s",
        "track=s", "format=s",
        "sort=s" => sub {
            ( $ACCEPTABLE_COLUMNS{$_[1]} )
            ? ( $options{$_[0]} = $_[1] )
            : ( die("Invalid field in --sort parameter, stop.\n") )
        },
        "columns=s" => sub {
            $options{$_[0]} = ();
            @{$options{'columns'}} = split(/,/, $_[1]);
            foreach (@{$options{'columns'}}) {
                if (not $ACCEPTABLE_COLUMNS{$_}) {
                    die("Invalid fields in --columns parameter, stop.\n")
                }
            }
        }
    ) or die("Can't parse command line arguments, stop.\n");

    return (\%filters, \%options);
}

sub get_library {
    my @library;

    while (<>) {
        push(@library, {%+}) if (m/
                ^[\.]\/
                (?<band>[^\/]+)\/
                (?<year>\d+)\s[-]\s(?<album>[^\/]+)\/
                (?<track>[^\/]+)[\.](?<format>\w+)\n?$
                /x)
    }

    return \@library;
}

1;
