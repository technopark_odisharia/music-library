package Local::MusicLibrary;

use strict;
use warnings;
use Local::MusicLibrary::Parser;
use Local::MusicLibrary::Printer;

# DEBUG
use DDP;
use Data::Dumper;

=encoding utf8

=head1 NAME

Local::MusicLibrary - core music library module

=head1 VERSION

Version 1.00

=cut

our $VERSION = '1.00';

=head1 SYNOPSIS

=cut
sub music_lib {
    my ($filters, $opts) = Local::MusicLibrary::Parser::get_options;
    my $lib  = Local::MusicLibrary::Parser::get_library;

    Local::MusicLibrary::Printer::print_table($lib, $filters, $opts);
}

1;
